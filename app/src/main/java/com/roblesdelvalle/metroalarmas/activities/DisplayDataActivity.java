package com.roblesdelvalle.metroalarmas.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.roblesdelvalle.metroalarmas.Adapters.AccountsAdapter;
import com.roblesdelvalle.metroalarmas.R;
import com.roblesdelvalle.metroalarmas.models.Information;

import org.json.JSONException;
import org.json.JSONObject;

public class DisplayDataActivity extends AppCompatActivity {

    public static final String INFO = "info";

    Gson gson = new Gson();
    Information information;
    
    //Views
    TextView tvId;
    TextView tvName;
    TextView tvEmail;
    TextView tvCount;
    RecyclerView rvAccounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_data);

        Bundle extras = getIntent().getExtras();
        if (extras == null){
            finish();
            return;
        }

        String response = extras.getString(DisplayDataActivity.INFO, null);
        if (response == null){
            finish();
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(response).getJSONObject("info");
            information = gson.fromJson(jsonObject.toString(), Information.class);
        } catch (JSONException e) {
            e.printStackTrace();
            finish();
            return;
        }

        //Binding views
        tvId = (TextView) findViewById(R.id.tvId);
        tvName = (TextView) findViewById(R.id.tvName);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvCount = (TextView) findViewById(R.id.tvCount);
        rvAccounts = (RecyclerView) findViewById(R.id.rvAccounts);
        
        
        //Fill views
        tvId.setText(information.getId());
        tvName.setText(information.getName());
        tvEmail.setText(information.getEmail());
        tvCount.setText(information.getCount());

        AccountsAdapter adapter = new AccountsAdapter(DisplayDataActivity.this, information.getAccounts());
        rvAccounts.setAdapter(adapter);
        rvAccounts.setLayoutManager(new LinearLayoutManager(DisplayDataActivity.this, LinearLayoutManager.VERTICAL, false));
        rvAccounts.addItemDecoration(new DividerItemDecoration(DisplayDataActivity.this, DividerItemDecoration.VERTICAL));


    }
}
