package com.roblesdelvalle.metroalarmas.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.roblesdelvalle.metroalarmas.R;
import com.roblesdelvalle.metroalarmas.utils.ConsumeWS;
import com.roblesdelvalle.metroalarmas.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private Button bOK;
    private TextInputLayout tilId, tilPassword;
    private TextInputEditText tietId, tietPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //binding Views
        tilId = (TextInputLayout) findViewById(R.id.tilId);
        tilPassword = (TextInputLayout) findViewById(R.id.tilPassword);
        tietId = (TextInputEditText) findViewById(R.id.tietId);
        tietPassword = (TextInputEditText) findViewById(R.id.tietPassword);
        bOK = (Button) findViewById(R.id.bOk);


        //setting Listeners
        bOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String id = tietId.getText().toString().trim();
                String password = tietPassword.getText().toString().trim();

                //clear errors
                tilId.setErrorEnabled(false);
                tilId.setError(null);
                tilPassword.setErrorEnabled(false);
                tilPassword.setError(null);

                if (id.equals("")){
                    tilId.setError("Diligencie este campo");
                    return;
                }

                if (password.equals("")){
                    tilPassword.setError("Diligencie este campo");
                    return;
                }

                //Consultar servicio
                final ProgressDialog progressDialog = Utils.showProgressDialog(MainActivity.this, getString(R.string.loading), getString(R.string.wait));
                ConsumeWS.consultData(id, password, new ConsumeWS.OnCompleteRequest() {
                    @Override
                    public void onSuccess(String response) {

                        if (response == null){
                            Utils.showDialog(MainActivity.this, getString(R.string.error), getString(R.string.error_ocurred));
                            if (!MainActivity.this.isFinishing()){
                                progressDialog.dismiss();
                            }
                            return;
                        }

                        try {
                            JSONObject jsonResponse = null;
                            jsonResponse = new JSONObject(response);
                            if (!jsonResponse.getBoolean("success")){
                                Utils.showDialog(MainActivity.this, getString(R.string.error), getString(R.string.no_information));
                                if (!MainActivity.this.isFinishing()){
                                    progressDialog.dismiss();
                                }
                                return;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.showDialog(MainActivity.this, getString(R.string.error), getString(R.string.error_ocurred));
                            if (!MainActivity.this.isFinishing()){
                                progressDialog.dismiss();
                            }
                            return;
                        }

                        Intent intent = new Intent(MainActivity.this, DisplayDataActivity.class);
                        intent.putExtra(DisplayDataActivity.INFO, response);
                        startActivity(intent);

                        if (!MainActivity.this.isFinishing()){
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onError() {

                        Utils.showDialog(MainActivity.this, getString(R.string.error), getString(R.string.error_ocurred));

                        if (!MainActivity.this.isFinishing()){
                            progressDialog.dismiss();
                        }

                    }
                });
            }
        });
    }
}
