package com.roblesdelvalle.metroalarmas.Adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.roblesdelvalle.metroalarmas.R;
import com.roblesdelvalle.metroalarmas.models.Account;

import java.util.List;

public class AccountsAdapter extends RecyclerView.Adapter<AccountsAdapter.AccountViewHolder> {

    Activity activity;
    List<Account> list;

    public AccountsAdapter(Activity activity, List<Account> list) {
        this.activity = activity;
        this.list = list;
    }

    public class AccountViewHolder extends RecyclerView.ViewHolder {

        public TextView tvAccountNumber;
        public TextView tvName;
        public TextView tvAddress;
        public TextView tvNotifications;

        public AccountViewHolder(View itemView) {
            super(itemView);

            tvAccountNumber = (TextView) itemView.findViewById(R.id.tvAccountNumber);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvAddress = (TextView) itemView.findViewById(R.id.tvAddress);
            tvNotifications = (TextView) itemView.findViewById(R.id.tvNotifications);
        }
    }

    @NonNull
    @Override
    public AccountViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(activity).inflate(R.layout.viewholder_account, parent, false);
        return new AccountViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AccountViewHolder holder, int position) {

        Account account = list.get(position);
        holder.tvAccountNumber.setText(account.getName());
        holder.tvName.setText(account.getAccountNumber());
        holder.tvAddress.setText(account.getAddress());
        holder.tvNotifications.setText(account.getNotifications());
    }

    @Override
    public int getItemCount() {
        return list == null? 0: list.size();
    }
}
