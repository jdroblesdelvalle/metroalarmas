package com.roblesdelvalle.metroalarmas.utils;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.roblesdelvalle.metroalarmas.MyApplication;

public class MyVolley {
    private static MyVolley singleton;
    private RequestQueue requestQueue;
    private Context context;

    private MyVolley(Context context) {
        this.context = context;
        requestQueue = getRequestQueue();
    }

    public static MyVolley getInstance() {
        if (singleton == null){
            singleton = new MyVolley(MyApplication.getInstance().getApplicationContext());
        }
        return singleton;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null){
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

    public void addToRequestQueue(Request request){
        getRequestQueue().add(request);
    }
}