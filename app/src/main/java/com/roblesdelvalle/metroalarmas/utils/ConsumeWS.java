package com.roblesdelvalle.metroalarmas.utils;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.roblesdelvalle.metroalarmas.config.Config;

import org.json.JSONException;
import org.json.JSONObject;

public class ConsumeWS {

    public static void consultData(String id, String password, final OnCompleteRequest onCompleteRequest){

        //String response = null;

        //Consulting WS
        final JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("Id", id);
            jsonRequest.put("Password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request request = new MyStringRequest(jsonRequest.toString(),
                Request.Method.POST,
                Config.WEB_SERVICE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        onCompleteRequest.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onCompleteRequest.onError();
            }
        });
        MyVolley.getInstance().addToRequestQueue(request);


        //Simulation
        /*response = "{\"success\":true,\"message\":\"Validación OK\",\"info\":{\"id\":\"800233036\",\"name\":\"PRUEBAS APP\",\"email\":null,\"password\":\"test\",\"accessToken\":null,\"accounts\":[{\"accountNumber\":\"01013732\",\"name\":\"PRUEBAS PANEL NUEVOS\",\"address\":\"Carrera 78 45 a 94\",\"notifications\":0},{\"accountNumber\":\"02013732\",\"name\":\"PRUEBAS PANEL NUEVOS\",\"address\":\"Carrera 78 45 a 94\",\"notifications\":0}],\"count\":2,\"so\":null,\"apiSdk\":null}}";
        return Utils.isJSONValid(response)? response: null;*/
    }

    public interface OnCompleteRequest {

        void onSuccess(String response);
        void onError();
    }
}
