package com.roblesdelvalle.metroalarmas.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.app.AlertDialog;

import com.roblesdelvalle.metroalarmas.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Utils {

    public static AlertDialog showDialog(Activity activity, String title, String message){

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.label_ok), null);

        AlertDialog dialog = builder.create();
        if (!activity.isFinishing()){
            dialog.show();
        }
        return dialog;
    }

    public static ProgressDialog showProgressDialog(Activity activity, String title, String message){

        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        if (!activity.isFinishing()){
            progressDialog.show();
        }
        return progressDialog;
    }

    //Method to check is string is a valid JSON
    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
}
