package com.roblesdelvalle.metroalarmas.utils;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.Map;

public class MyStringRequest extends StringRequest {

    String jsonString;

    public MyStringRequest(String jsonToSend, int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        this.jsonString = jsonToSend;
    }

    public MyStringRequest(String jsonToSend, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
        this.jsonString = jsonToSend;
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        return jsonString.getBytes();
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headersMap = new HashMap<>();
        headersMap.put("Authorization", "Basic YXBwOk0zdHIwMjAxNQ==");
        return headersMap;
    }
}
