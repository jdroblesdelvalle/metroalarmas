package com.roblesdelvalle.metroalarmas.models;

import java.util.List;

public class Information {

    private String id;
    private String name;
    private String email;
    private String password;
    private String accessToken;
    private List<Account> accounts;
    private String count;
    private String so;
    private String apiSdk;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public String getApiSdk() {
        return apiSdk;
    }

    public void setApiSdk(String apiSdk) {
        this.apiSdk = apiSdk;
    }
}
